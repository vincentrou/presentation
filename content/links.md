## Quelques liens

* La Théorie Relative de la Monnaie
  * [trm.creationmonetaire.info](http://trm.creationmonetaire.info)
* Forum de la monnaie libre
  * [forum.monnaie-libre.fr](https://forum.monnaie-libre.fr)
* Le logiciel libre Duniter
  * [www.duniter.org](https://www.duniter.org)
* Aperçu de la Ğ1 avec Cesium
  * [g1.duniter.fr](https://g1.duniter.fr)
