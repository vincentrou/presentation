# Monnaie libre

et sa première expérimentation avec Duniter

![duniter](image/duniter-logo.svg)<!-- .element width="20%" style="border: 0; background: None; box-shadow: None"-->
![g1](image/Logo-g1.flare.svg)<!-- .element width="20%" style="border: 0; background: None; box-shadow: None"-->
![cesium](image/logo.large.cesium.dune.svg)<!-- .element width="20%" style="border: 0; background: None; box-shadow: None"-->

===

## La monnaie

«C’est pourquoi on a recours à la monnaie, qui est, pour ainsi dire un **intermédiaire**.

Elle **mesure** tout, la valeur supérieure d’un objet et la valeur inférieure d’un autre, par exemple combien il faut de chaussures pour équivaloir à une maison ou à l’alimentation d’une personne, faute de quoi, il n’y aura ni **échange**, ni **communauté** de rapports.»

**Aristote**
"Ethique à Nicomaque", 300 av J.C. (trad. R. Bodéïs, GF)

---

![trois_producteurs](image/trois_producteurs_title.png)<!-- .element width="70%" -->

Note:
Figure du sou : https://www.le-sou.org/telechargements/documents/

===

## Création monétaire par la dette

* L'argent est émis par des banques privées
* Les banques prêtent de l'argent suivant des critères arbitraires
* Un intérêt est ajouté à cette dette

---

## Conséquences

* Recheche du profit en permanence
  * pour rembourser les dettes
* Inégalité des individus devant la création monétaire

===

## La monnaie libre

* Définie dans la Théorie Relative de la Monnaie (TRM) par Stéphane Laborde en 2010
* Équité de la création monétaire pour les individus
  * dans l'espace
  * dans le temps

---

## Les 4 libertés économiques

1. La liberté de choix de son système monétaire
2. La liberté d’utiliser les ressources
3. La liberté d’estimation et de production de toute valeur économique
4. La liberté d’échanger, comptabiliser, afficher ses prix “dans la monnaie”

---

## Équité dans la création monétaire

* Création monétaire via un Dividende Universel (DU)
* Augmentation du DU dans le temps indexé sur l'espérance de vie <!-- .element: class="fragment" data-fragment-index="1" -->

*$$
  DU(t) = c\frac{M(t)}{N(t)}
$$
avec **c** compris entre 5 et 10% par an pour une espérance de vie de 80 ans*  <!-- .element: class="fragment" data-fragment-index="2" -->

---

![M](image/M.png)

---

![M](image/DU.png)

===

### Ğ1 : 1ère expérimentation réelle

* La Ğ1 (prononcé «june») existe depuis mars 2017
* Duniter est le logiciel qui gère cette crypto-monnaie libre <!-- .element: class="fragment" data-fragment-index="1" -->
* Les règles de la création monétaire sont figées et transparentes <!-- .element: class="fragment" data-fragment-index="2" -->

![g1](image/Logo-g1.flare.svg)<!-- .element width="25%" style="border: 0; background: None; box-shadow: None"-->

---

### J'accepte la Ğ1 !

* Co-création monétaire via un DU (~10 Ğ1/jour actuellement)
* Échange d'argent via un logiciel client (Cesium, Sakia, Silkaj)
* Annonce de bien et de service sur :
  * Ğchange : [www.gchange.fr](https://www.gchange.fr)
  * Ğannonce : [gannonce.duniter.org](https://gannonce.duniter.org)

![cesium](image/logo.large.cesium.dune.svg)<!-- .element width="18%" style="border: 0; background: None; box-shadow: None"-->

