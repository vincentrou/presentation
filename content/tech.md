### Caratéristique de Duniter

  * Licence : AGPLv3
  * Language : TypeScript
  * Gitlab : [git.duniter.org](https://git.duniter.org/nodes/typescript/duniter)

---

### Comment fonctionne Duniter ?

* Pour co-créer la Ğ1, il faut devenir membre de la toile de confiance
* La sécurité de Duniter est assurée par l'utilisation d'une blockchain décentralisée

![duniter](image/duniter-logo.svg)<!-- .element width="25%" style="border: 0; background: None; box-shadow: None"-->

---

### Toile de confiance

* 5 certifications pour postuler comme membre
* Un nouveau membre ne doit pas à être à plus de 5 pas de 80% des membres référents <!-- .element: class="fragment" data-fragment-index="1" -->
* Notion de temps dans l'émission et la validité des certifications <!-- .element: class="fragment" data-fragment-index="2" -->

---

### Règle de distance

![wot-sybil](image/wot-sybil.jpg)

---

![wot](image/wot.jpg)<!-- .element width="70%" -->

---

### La chaine de block Duniter

Type de document possible :

* DU
* Certification
* Membership (IN/OUT)
* Transaction
* Revocation

---

### Difficulté personnalisée

*Synchronisation des noeuds pour l'écriture de la blockchain via une **preuve de travail** *

<div>

* Difficulté commune
* Un membre = un vote
* Plus de puissance = plus de blocs trouvés = augmentation du handicap
* Un bloc trouvé = exclusion temporaire</div> <!-- .element: class="fragment" data-fragment-index="1" -->

